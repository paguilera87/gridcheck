#!/bin/python3.6
import shlex
import fileinput
import subprocess
import os
import glob
import sys
import argparse
import json
import ast

def getTierURL( tiername ):
	if tiername == 'CNAF' or tiername == 'cnaf' :
		url = 'srm://storm-fe-archive.cr.cnaf.infn.it:8444/agata/'
		return url
	elif tiername == 'CCIN2P3' or tiername == 'ccin2p3':
		url = 'srm://ccsrm02.in2p3.fr:8443/pnfs/in2p3.fr/data/agata'
		return url
	else :
		return ""


def getExperimentFolder( name ):
	transfertfolder = '/agatagrid/runs/{}'.format(name)
	if os.path.exists(transfertfolder) :
		return transfertfolder
	agatafolder = '/agatadisks/{}'.format(name)
	if os.path.exists(agatafolder) :
		print('--- Experiment folder founded in agata disks, needs to be copied first: {}'.format(agatafolder))
		return ''
	else :
		print('--- Experiment folder not founded in /agatagrid nor in /agatadisks, check typo: {}'.format(name))
		return ''

def getRemoteSize( filename, serverurl):  # filename without /agatagrid/runs and serverurl upto agata
	if filename=='' or serverurl=='':
		return ''
	cmd = 'gfal-ls -lH {}{} >> /dev/null'.format(url,fnamenodisc)
	try:
		output = subprocess.run( cmd, shell=True,check=True )
	except subprocess.CalledProcessError:
		print('')		

def getLocalFile( filename ):
	if filename == '': return ''
	thename = '/agatagrid/runs{}'.format( filename )	
	return thename

def getTierFile( filename, tiername):
	tierurl = getTierURL( tiername )
	if filename=="" or tierurl=="" : return ''
	thename = '{}{}'.format	(tierurl,filename)
	return thename

def getLocalSize( filename ):
	fulllocalname = getLocalFile( filename )
	if fulllocalname == '': return ''
	cmd = 'ls -Llh {}'.format(fulllocalname)
#	print('--- cmd local size:',cmd)
	try:
		output = subprocess.check_output(shlex.split( cmd ) , encoding='utf-8')
		thesize = output.split()[4]
		return thesize
	except subprocess.CalledProcessError:
		return '-1'

def getTierSize( filename, url ):
	fulltiername = getLocalFile( filename )
	if fulltiername == '': return ''
	cmd = 'gfal-ls -lH {}'.format(fulltiername)
#	print('--- the cmd to check grid:',cmd)
	try:
		output = subprocess.check_output(shlex.split( cmd ) , encoding='utf-8')
		thesize = output.split()[4]
		#thesize = output
		return thesize
	except subprocess.CalledProcessError:
		return '-1'

def getLocalMD5( filename ):
	fulllocalname = getLocalFile( filename )
	if fulllocalname == '': return ''
	cmd = 'md5sum {}'.format(fulllocalname)
#	print('--- cmd local size:',cmd)
	try:
		output = subprocess.check_output(shlex.split( cmd ) , encoding='utf-8')
		thesize = output.split()[0]
		return thesize
	except subprocess.CalledProcessError:
		return '-1'

def getTierMD5( filename, url ):
	fulltiername = getLocalFile( filename )
	if fulltiername == '': return ''
	cmd = 'gfal-sum -v {}'.format(fulltiername)
#	print('--- the cmd to check grid:',cmd)
	try:
		output = subprocess.check_output(shlex.split( cmd ) , encoding='utf-8')
		thesize = output.split()[4]
		#thesize = output
		return thesize
	except subprocess.CalledProcessError:
		return '-1'

class griddata:
	filename = ""
	tiername = ""
	localpath = ""
	localsize = ""
	tierpath = "" 
	tiersize = ''
	tierstatus = ""
	nfile=''
	def print(self):
		print('----------------')
		print('--- filename:   ',self.filename)	
		print('--- tiername:   ',self.tiername)	
		print('--- local path: ',self.localpath)
		print('--- local size: ',self.localsize)
		print('--- tier path:  ',self.tierpath)
		print('--- tier size:  ',self.tiersize)
		print('--- tier status:  ',self.tierstatus)
		print('----------------')
	def set_filename(self, filename):
		if filename == '' : return
		self.filename = filename
	def get_filename(self ):
		return self.filename
	def set_tier(self, tiername):
		if tiername == '' : return
		self.tiername = tiername
	def set_localpath(self,localpath):
		self.localpath = localpath
	def get_localpath(self):
		return self.localpath 
	def set_localsize(self,localsize):
		self.localsize = localsize
	def get_localsize(self):
		return self.localsize 
	def set_tierpath(self,tierpath):
		self.tierpath = tierpath
	def set_tiersize(self,tiersize):
		self.tiersize = tiersize
	def get_tiersize(self):
		return self.tiersize
	def set_tierstatus(self,tierstatus):
		self.tierstatus = tierstatus
	def get_tierstatus(self):
		return self.tierstatus
	def set_nfile(self,n):
		self.nfile = n
	def get_nfile(self):
		return self.nfile
	def is_ready(self):
		#if self.tierstatus=='Copied': return True
		if self.localsize == '-1' or self.localsize=='': return False
		if self.tiersize == '-1' or self.tiersize=='': return False
		if self.tiersize!=self.localsize : return False
		self.tierstatus='Copied'
		return True
	def update(self):
		if self.filename=='' : return
		self.set_localpath( getLocalFile(self.filename) )
		self.set_localsize( getLocalSize(self.filename) )
		if self.tiername=='': return
		self.set_tierpath( getTierFile( self.filename, self.tiername) )
		self.set_tiersize( getTierSize( self.filename, self.tiername) )
		if self.tiersize!='-1' and self.tiersize==self.localsize:
			self.set_tierstatus('Copied')	
		else :
			self.set_tierstatus('notReady')	
	def __init__(self, filename, tiername , localpath='', localsize='-1', tierpath='',tiersize='-1',tierstatus='notReady', nfile=-1 ):
		self.set_filename( filename )	
		self.set_tier( tiername )
		if localpath=='': self.set_localpath( getLocalFile(filename) )
		else : self.set_localsize( localsize )
		if localsize!='' : self.set_localsize( localsize)
		else : self.set_localsize( '-1')
		if tierpath=='': self.set_tierpath( getTierFile(filename , tiername) )
		else : self.set_tierpath( tierpath)
		if tiersize!='' : self.set_tiersize( tiersize)
		else : self.set_tiersize( '-1') 
		if tierstatus!='' : self.set_tierstatus( tierstatus)
		else : self.set_tierstatus( 'notReady' )
		if nfile!='-1' : self.set_nfile( nfile)
		else : self.set_nfile('-1')
		#self.update()



class statusdatabase:
	localfolderfound = False
	nlines=0
	inconsistent=0
	notready=0
	passed=0
	total=0
	def set_statusfile(self, filename):
		if filename == '' : return
		self.filename = filename
	def set_tier(self, tiername):
		if tiername == '' : return
		self.tiername = tiername
	def set_experiment(self, expname):
		if expname == '' :
			localfolderfound = False
			return
		else :
			efolder =  getExperimentFolder(expname)
			if os.path.isdir(efolder) :
				self.localfolderfound = True
			else : 
				self.localfolderfound = False
		self.experiment = expname
	def get_line_formated(self, thedata):
		if thedata==None : return ''
		theline = '{\'%s\':{\'status\':\'%s\',\'localsize\':\'%s\',\'tiersize\':\'%s\',\'nfile\':\'%s\'}}' % ( thedata.get_filename(),thedata.get_tierstatus(), thedata.get_localsize(), thedata.get_tiersize(), thedata.get_nfile() )
		return theline
	def create_statusfile(self, filename):
		if os.path.isfile(filename) : 
			print("status file already exists: ", filename)
			with open(filename, 'r') as fp:
				x = len(fp.readlines())
			print('Total lines:', x) # 8
			self.nlines=x
			return
		tmpfile = 'checklogs/.tmpfilelist.dat'
		expfolder = getExperimentFolder(self.experiment)
		cmd = 'find -L {} -type f  | grep "SRM_"  >  {}'.format( expfolder ,tmpfile )
		os.system(cmd)
		with open(tmpfile, 'r') as fp:
			x = len(fp.readlines())
		print('Total lines:', x) # 8
		self.nlines = x
		f = open(tmpfile, "r")
		counter = x
		for line in f:
			if line== ' ': continue
			tempfilename ='/{}'.format(line.strip('/agatagrid/runs').rstrip('\n')) 
			thedata = griddata( tempfilename , self.tiername )
			#thedata = griddata(line.rstrip('\n') , self.tiername)
			thedata.set_nfile( counter)
			formatedline = self.get_line_formated(thedata)
#			print(formatedline)
			with open(filename, 'a') as ff: 
				ff.write( formatedline+'\n' ) 
				counter -=1
#				if counter+10<x : break
		os.system('rm {}'.format(tmpfile))
	def update_status_in_statusfile(self ):
		print('--- updating status in statusfile')
		if not os.path.isfile(self.filename) : raise FileNotFoundError
		f = open(self.filename, "r")
		tmpfile = '{}.tmp'.format(self.filename)
		if os.path.isfile(tmpfile) : os.system('rm {}'.format(tmpfile))
		for line in f:
			if line== ' ': continue
			data = ast.literal_eval(line)
			for key, value in data.items():
#				print('key:',key)
				thedata = griddata(key,self.tiername)
				thedata.set_localsize( value['localsize'] )
				thedata.set_tiersize( value['tiersize'] )
				thedata.set_tierstatus( value['status'] )
				thedata.set_nfile( value['nfile'] )
				if thedata.is_ready() and thedata.get_tierstatus()!= value['status']: 
					print('{} --- New Status: {}:'.format(thedata.get_nfile(),thedata.get_tierstatus()),key)
	#			if value['status'] == 'Copied': continue
	#			if not thedata.is_ready() : continue
		#		self.update_entry( key, thedata)
				formatedline = self.get_line_formated(thedata)
				with open(tmpfile, 'a') as ff: 
					ff.write( formatedline+'\n' ) 
		os.system('mv {} {}'.format(tmpfile, self.filename))
	def update_localsizes_in_statusfile(self ):
		if self.localfolderfound : 
			print('--- updating local sizes')
		else : 
			print('--- Local folder not found, cannot update local sizes')
			return
		if not os.path.isfile(self.filename) : raise FileNotFoundError
		f = open(self.filename, "r")
		tmpfile = '{}.tmp'.format(self.filename)
		if os.path.isfile(tmpfile) : os.system('rm {}'.format(tmpfile))
		for line in f:
			if line== ' ': continue
			data = ast.literal_eval(line)
			for key, value in data.items():
#				print('key:',key)
				thedata = griddata(key,self.tiername)
				thedata.set_localsize( value['localsize'] )
				thedata.set_tiersize( value['tiersize'] )
				thedata.set_tierstatus( value['status'] )
				thedata.set_nfile( value['nfile'] )
				#if value['localsize'] != '' and  value['localsize']!='-1': continue
				newsize = getLocalSize( key )
				if newsize!= '' and newsize !='-1': thedata.set_localsize( newsize)
#				if newsize!='' and newsize!='-1' : print('{} +++ Local Size {}:'.format(thedata.get_nfile(),newsize),key)
#				else : print('{} --- Local Size {}:'.format(thedata.get_nfile(),newsize),key)
#				self.update_entry( key, thedata)
				formatedline = self.get_line_formated(thedata)
				with open(tmpfile, 'a') as ff: 
					ff.write( formatedline+'\n' ) 
#				thedata.print()
		os.system('mv {} {}'.format(tmpfile, self.filename))
	def update_tiersizes_in_statusfile(self):
		print('--- updating tier sizes')
		if not os.path.isfile(self.filename) : raise FileNotFoundError
		f = open(self.filename, "r")
		tmpfile = '{}.tmp'.format(self.filename)
		if os.path.isfile(tmpfile) : os.system('rm {}'.format(tmpfile))
		for line in f:
			if line== ' ': continue
			data = ast.literal_eval(line)
			for key, value in data.items():
				thedata = griddata(key,self.tiername)
				thedata.set_localsize( value['localsize'] )
				thedata.set_tiersize( value['tiersize'] )
				thedata.set_tierstatus( value['status'] )
				thedata.set_nfile( value['nfile'] )
				if value['tiersize'] == '' or  value['tiersize']=='-1':
					newsize = getTierSize( key, self.tiername )
					if newsize != '' and newsize !='-1': thedata.set_tiersize( newsize)
					if thedata.get_tiersize()!='' and thedata.get_tiersize()!='-1' : print('{} +++ Size {} from tier {}:'.format(thedata.get_nfile(),thedata.get_tiersize(),self.tiername),key)
					else : print('{} --- Size {} from tier {}:'.format(thedata.get_nfile(),thedata.get_tiersize(),self.tiername),key)
				#thenewline = self.get_line_formated( thedata)
				#self.update_entry( key, thedata)
				formatedline = self.get_line_formated(thedata)
				with open(tmpfile, 'a') as ff: 
					ff.write( formatedline+'\n' ) 
				#thedata.print()
		os.system('mv {} {}'.format(tmpfile, self.filename))
	def __init__(self, filename, experiment, tiername):
		if filename=='' or experiment=='' or tiername=='': return 
		self.set_statusfile( filename )
		self.set_tier( tiername )
		self.set_experiment( experiment )
		self.create_statusfile( filename )
		#self.update_names_in_statusfile( filename )
	def getDataLine(self, filename):
		if filename=='': raise FileNotFoundError
		#file2search = getLocalFile( filename )
		#if file2search=='': raise FileNotFoundError
		f = open(self.filename, "r")
		for line in f:
			if  line== '': continue
			data = ast.literal_eval(line)
			for key, value in data.items() :
#				print ('key:',key,'filename:',file2search)
				#if key!=file2search: continue
				if key!=filename: continue
				#print('line:',line)
				return line
		return ''
	def getData(self, filename ):
		line = self.getDataLine( filename )
		if line =='' : return None
		data = ast.literal_eval(line)
		for key, value in data.items():
			thedata = griddata(filename,self.tiername)
			thedata.set_localsize( value['localsize'] )
			thedata.set_tiersize( value['tiersize'] )
			thedata.set_tierstatus( value['status'] )
			return thedata
		return None
	def printsummary(self):
		print('--- Printing summary')
		if not os.path.isfile(self.filename) : raise FileNotFoundError
		f = open(self.filename, "r")
		for line in f:
			if line== ' ': continue
			data = ast.literal_eval(line)
			for key, value in data.items():
				thedata = griddata(key,self.tiername)
				thedata.set_localsize( value['localsize'] )
				thedata.set_tiersize( value['tiersize'] )
				thedata.set_tierstatus( value['status'] )
				if thedata.get_tierstatus()=='Copied' and thedata.get_localsize()!='-1' and thedata.get_tiersize()!='-1' and thedata.get_localsize()==thedata.get_tiersize():
					self.passed+=1
					print('+++ {}.{}.{}.{} Size {} from tier:'.format(self.total,self.passed,self.notready,self.inconsistent,thedata.get_tiersize()),key)
				elif thedata.get_tierstatus()=='notReady' and thedata.get_localsize()!='-1' and thedata.get_tiersize()=='-1' :
					self.notready+=1
					print('--- {}.{}.{}.{} Size {} from tier:'.format(self.total,self.passed,self.notready,self.inconsistent,thedata.get_tiersize()),key)
				else : 
					self.inconsistent+=1
					print('xxx {}.{}.{}.{} Size {} from tier:'.format(self.total,self.passed,self.notready,self.inconsistent,thedata.get_tiersize()),key)
				self.total+=1
		print('\n === N.Files {}. Total {}. Already copied {}. notReady {}. inconsistent {} ==='.format(self.nlines,self.total,self.passed,self.notready,self.inconsistent))
	def print( self ):
		print('================')
		print('=== status file: ',self.filename)	
		print('=== tiername:    ',self.tiername)	
		print('=== experiment:  ',self.experiment)
		if self.localfolderfound : 
			print('=== local folder found:  ', getExperimentFolder(self.experiment) )
		else :
			print('=== local folder not found  ',getExperimentFolder(self.experiment))
		print('================')
	
		


def main():
	print('\n--- Manual check for files copied to grid --- ')
	parser = argparse.ArgumentParser(description='Script to test if all files were transferred to grid')	
	parser.add_argument('--exp',
					nargs=1,
					required=True,
					dest='exp',
					help='The experiment name (example: AGATAD_P2_EXP_001')
	parser.add_argument('--tier',
					nargs=1,
					required=True,
					dest='tier',
					help='The TIER to check (CNAF or CCIN2P3')
	args = parser.parse_args()
	print(args)
	expfolder = getExperimentFolder(args.exp[0])
	if expfolder == '':
		print('\n Cannot check the experiment ',expfolder)
#		sys.exit()
	else : print('\n Experiment folder:',expfolder)
	url = getTierURL(args.tier[0])	
	if url == '':
		print('\n URL cannot be get: ',url)
		sys.exit()
	
	statusfile = '/agatagrid/transfert/checklogs/manualcheck2_status_{}_{}.dat'.format(args.exp[0],args.tier[0])
	mystatusfile = statusdatabase(statusfile, args.exp[0], args.tier[0])
	mystatusfile.print()
	mystatusfile.update_localsizes_in_statusfile()
	mystatusfile.update_tiersizes_in_statusfile()
	mystatusfile.update_status_in_statusfile()
	mystatusfile.printsummary()
	mystatusfile.print()

def test1 () :	
	filename = '/AGATAD_P2_EXP_001/run_0027_29-05-2022_18h42m42s/Data/04C/SRM_AGATA_small_files.tar'	
	tiername = 'CNAF'
	
	print('Checking file ',filename)
	print('Checking tier ',tiername)

	print('\n Local name ', getLocalFile(filename))
	print('\n Local size ', getLocalSize(filename))
	print('\n Tier file name ', getTierFile(filename,tiername))
	print('\n Tier size ', getTierSize(filename,tiername))

def test2():
	filename = '/AGATAD_P2_EXP_001/run_0027_29-05-2022_18h42m42s/Data/04C/SRM_AGATA_small_files.tar'	
	tiername = 'CNAF'
	mydata = griddata(filename,tiername)
	mydata.print()

def test3():
	statusfile = 'checklogs/teststatusfile.dat'
	experiment = 'AGATAD_P2_EXP_001'
	tiername = 'CNAF'
	mystatusfile = statusdatabase(statusfile, experiment, tiername)
	mystatusfile.print()
	filename = '/AGATAD_P2_EXP_001/run_0027_29-05-2022_18h42m42s/Data/04C/SRM_AGATA_small_files.tar'	
	try:
		thedata = mystatusfile.getData(filename)
		print('\n Printing data:')
		if thedata==None : return
		thedata.print()
		prevstatus = thedata.get_tierstatus()
		thedata.update()
		thedata.print()
		newstatus = thedata.get_tierstatus()
		if newstatus!=prevstatus and newstatus=='Copied':
			mystatusfile.update_entry(filename,thedata)
	except FileNotFoundError:
		pass

def test4():
	statusfile = 'checklogs/teststatusfile.dat'
	experiment = 'AGATAD_P2_EXP_001'
	tiername = 'CNAF'
	mystatusfile = statusdatabase(statusfile, experiment, tiername)
	mystatusfile.print()
	filename = '/AGATAD_P2_EXP_001/run_0027_29-05-2022_18h42m42s/Data/04C/SRM_AGATA_small_files.tar'	
	try:
		thedata = mystatusfile.getData(filename)
		print('formated line:',mystatusfile.get_line_formated(thedata))
	except FileNotFoundError:
		pass

if __name__ == '__main__':
	main()
#	print('\n========================')
#	test1()
#	print('\n========================')
#	test2()
#	print('\n========================\n')
#	test3()
#	print('\n========================\n')
#	test4()
