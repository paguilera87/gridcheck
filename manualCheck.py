#!/bin/python3.6
import fileinput
import subprocess
import os
import glob
import sys
import argparse
import json


def getExperimentFolder( name ):
	transfertfolder = '/agatagrid/runs/{}'.format(name)
	#print('\nChecking experiment folder: {}'.format(transfertfolder))
	if os.path.exists(transfertfolder) :
	#	print('--- Experiment folder founded: {}'.format(transfertfolder))
		return transfertfolder
	#else :
	#	print('--- Experiment folder not founded, checking in agata')
	agatafolder = '/agatadisks/{}'.format(name)
	if os.path.exists(agatafolder) :
	#	print('--- Experiment folder founded in agata disks, needs to be copied first: {}'.format(agatafolder))
		return ''
	else :
	#	print('--- Experiment folder not founded in /agatagrid nor in /agatadisks, check typo: {}'.format(name))
		return ''


def fillstatusfile( filename , exp ):
	tmpfile = '.tmpfilelist.dat'
	expfolder = getExperimentFolder(exp)
	cmd = 'find -L {} -type f  | grep "SRM_"  >  {}'.format( expfolder ,tmpfile )
	os.system(cmd)
	filedict = {}
	f = open(tmpfile, "r")
	for line in f:
		if not line== ' ':
			filedict[line.rstrip('\n')] = 'notReady'	
	with open(filename, 'w') as f: 
		for key , value in filedict.items(): 
        		f.write('%s : %s\n' % (key, value))	
#	with open(filename, 'w') as convert_file:
#		convert_file.write(json.dumps( filedict ))
	os.system('rm {}'.format(tmpfile))

def getdict( filename ):
	filedict = {}
	f = open(filename, "r")
	for line in f:
		if  line== ' ': continue
		splittedline = line.rstrip('\n').split(" : ")
		key,value = splittedline[0],splittedline[1]
	#	print(key, value )
		if value == 'notReady':
			filedict[key] = value
	return filedict
	

def makelist( filename , exp , tier ):
	print('\nPreparing status file: ',filename)
	if not os.path.exists(filename) :
		print('--- Statusfile does not  exists, creating it')
		os.system('touch {}'.format(filename) )
		fillstatusfile( filename, exp );
	else:
		print('--- Statusfile already exists')



def getURL( tiername ):
	print('\nChecking TIER url: {}'.format(tiername))
	if tiername == 'CNAF' or tiername == 'cnaf' :
		url = 'srm://storm-fe-archive.cr.cnaf.infn.it:8444/agata/'
		print('--- TIER url: {}'.format(url))
		return url
	elif tiername == 'CCIN2P3' or tiername == 'ccin2p3':
		url = 'srm://ccsrm02.in2p3.fr:8443/pnfs/in2p3.fr/data/agata'
		print('--- TIER url: {}'.format(url))
		return url
	else :
		print('\n--- TIER name unknown: {} (can be CNAF or CCIN2P3)'.format(tiername))
		return ''


def replace_in_file(file_path, agatafile, old_status, new_status):
	with fileinput.input(file_path, inplace=True) as file:
		for line in file:
			new_line = line.replace('{} : {}'.format(agatafile,old_status), '{} : {}'.format(agatafile,new_status))
			print(new_line, end='')	

def updatelist( filename , url ):
	print('\nUpdating status for remaining files: \n')
	filesdict = getdict(filename)
	totalcheck = len(filesdict)
	currcheck = totalcheck
	passed = 0
	failed = 0
	for x in filesdict :
		fnamenodisc = x.replace("/agatagrid/runs","")
		cmd = 'gfal-ls -H {}{} >> /dev/null'.format(url,fnamenodisc)
		#print('\n --- Checking file: ', x)
		try:
			output = subprocess.run( cmd, shell=True,check=True )
			replace_in_file( filename, x, 'notReady', 'Copied')
			print('--- f:{} {}/{} File copied: {}'.format(failed,currcheck, totalcheck, x))
			passed += 1
		except subprocess.CalledProcessError:
			print('--- f:{} {}/{} File failed: {}'.format(failed,currcheck, totalcheck, x))
			failed += 1
		currcheck -= 1
	print('\nSummary: Check:{} Passed:{} Failed:{}'.format(totalcheck-currcheck, passed, failed))
		

def main():
	print('\n--- Manual check for files copied to grid --- ')
	parser = argparse.ArgumentParser(description='Script to test if all files were transferred to grid')	
	parser.add_argument('--exp',
					nargs=1,
					required=True,
					dest='exp',
					help='The experiment name (example: AGATAD_P2_EXP_001')
	parser.add_argument('--tier',
					nargs=1,
					required=True,
					dest='tier',
					help='The TIER to check (CNAF or CCIN2P3')
	args = parser.parse_args()
#	print(args)
	expfolder = getExperimentFolder(args.exp[0])
	if expfolder == '':
		print('\n Cannot check the experiment ',expfolder)
		sys.exit()
	url = getURL(args.tier[0])	
	if url == '':
		print('\n URL cannot be get: ',url)
		sys.exit()
	
	statusfile = '/agatagrid/transfert/checklogs/manualcheck_status_{}_{}.dat'.format(args.exp[0],args.tier[0])
	makelist( statusfile , args.exp[0] , args.tier[0] )
	updatelist( statusfile , url )

	

if __name__ == '__main__':
	main()
