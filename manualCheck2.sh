#!/bin/bash

experiment=$1
tier=$2
folder="/agatagrid/runs/${experiment}"



echo " "
echo "################################"
#echo "Checking:  $folder to $tier";
#
#if [ -d "$folder" ]
#then
#	echo "Folder found: $folder"	
#else
#	echo "Folder not found: $folder";
#	exit 1;
#fi


logfile="/agatagrid/transfert/logs/${experiment}_copy_${tier}.log"
if [[ -f "${logfile}" ]]; then
	echo -e "\e[0;32m Log file found: $logfile \e[0m"
else
	echo -e "\e[0;31m Log file not found: $logfile \e[0m"
	exit 1;
fi


cat $logfile | while read line
do
	if [[ "${line}" == *"rror"* ]] 
	then
# 		echo "Problem in file: $line"
		IFS="::" read name filename <<< $line
		filename=${filename#":put "}
		#echo "Problem in file: $filename"
		sublist=`grep ${filename} ${logfile} | grep "uccess"`
		#echo $sublist
		if [[ "${sublist}" == "" ]]
		then
			echo -e "\e[0;31m  Error remaining in: ${filename} \e[0m"
		fi
	fi

done



exit 0



templocalfiles="manualCheck_localfiles_${experiment}.txt"
find ${folder} -type f  | grep -v ".tar" | grep -v ".py" | grep -v ".conf" | grep -v ".cal" | grep -v ".ban" | grep -v ".save" | grep -v ".thres" | grep -v ".spec" | grep -v ".matr" | grep -v ".aver" | grep ".cdat" >  $templocalfiles
if [[ -f "$templocalfiles" ]]; then
	echo "List of local files created: $templocalfiles"
else
	echo "List of files was not created. Interrupting"
	exit 1;
fi



commandnofile="gfal-ls -H srm://ccsrm02.in2p3.fr:8443/pnfs/in2p3.fr/data/agata/"
tempfilecorrect="manualCheck_correctfiles_${experiment}.txt"
tempfilewitherror="manualCheck_fileswitherrors_${experiment}.txt"
rm ${tempfilecorrect}
rm ${tempfilewitherror}
cat $templocalfiles | while read line
do
	filenoprefix=`echo $line | sed -e "s/\/agatagrid\/runs\///"`
#	echo $filenoprefix
	command=${commandnofile}${filenoprefix}
	response=`${command} 2>&1`
#	echo "== Response: $response"
	if [[ "${response}" == *"rror"* ]] 
	then
  		echo "Problem in file: $line"
		echo $line >> $tempfilewitherror
	else
		echo "File ok: $line"
		echo $line >> $tempfilecorrect
	fi

done

