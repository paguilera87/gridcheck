#!/bin/bash

experiment=$1
server=$2

url=""
if [[ $server == "CNAF" ]]
then
	echo "** Checking on server: $server"
	url="srm://storm-fe-archive.cr.cnaf.infn.it:8444/agata/"
	echo "** $url "
fi
if [[ $server == "CCIN2P3" ]]
then
	echo "** Checking on server: $server"
	url="srm://ccsrm02.in2p3.fr:8443/pnfs/in2p3.fr/data/agata/"
	echo "** $url "
fi

if [[ $url == "" ]]
then
	echo "Missing a valid server (CNAF or CCIN2P3), given: $server "
	exit 0
fi



folder="/agatagrid/runs/${experiment}"

echo " "
echo "################################"
echo "Checking:  $folder";

if [ -d "$folder" ]
then
	echo "Folder found: $folder"	
else
	echo "Folder not found: $folder";
	exit 1;
fi


templocalfiles="manualCheck_localfiles_${experiment}_${server}.txt"
#find ${folder} -type f  | grep -v ".tar" | grep -v ".py" | grep -v ".conf" | grep -v ".cal" | grep -v ".ban" | grep -v ".save" | grep -v ".thres" | grep -v ".spec" | grep -v ".matr" | grep -v ".aver"  >  $templocalfiles
find ${folder} -type f  | grep "SRM_"  >  $templocalfiles
if [[ -f "$templocalfiles" ]]; then
	echo "- List of local files created: $templocalfiles"
else
	echo "- List of files was not created. Interrupting"
	exit 1;
fi

#exit 0

commandnofile="gfal-ls -H ${url}"
tempfilecorrect="manualCheck_correctfiles_${experiment}_${server}.txt"
tempfilewitherror="manualCheck_fileswitherrors_${experiment}_${server}.txt"
rm ${tempfilecorrect}
rm ${tempfilewitherror}

echo "# Starting "
echo " "
cat $templocalfiles | while read line
do
	filenoprefix=`echo $line | sed -e "s/\/agatagrid\/runs\///"`
#	echo $filenoprefix
	command=${commandnofile}${filenoprefix}
	expectedresponse=${url}${filenoprefix}
#	echo $expectedresponse
	echo $command
	response=`${command} 2>&1`
#	echo "== Response: $response"
#	if [[ "${response}" == *"rror"* ]] 
	if [[ "${response}" != ${expectedresponse} ]] 
	then
  		echo -e "-- Problem in file: \e[0;31m $line \e[0m"
		echo    $line >> $tempfilewitherror
	else
		echo -e "-- File ok: \e[0;32m $line \e[0m"
		echo    $line >> $tempfilecorrect
	fi
done


echo "---------------------- "
if [ -f "$tempfilewitherror" ]
then
	echo -e "\e[0;31m == Some files have problems, check the list in file:  ${tempfilewitherror} \e[0m"
else 
	echo -e "\e[0;32m == All files listed were observed in GRID, check the list in file:  ${tempfilecorrect} \e[0m"
fi

